/*
 * @Author: tackchen
 * @Date: 2021-09-30 17:25:03
 * @LastEditors: tackchen
 * @FilePath: \gamematrix-h5-site\client\js-sdk\tacl-ui\src\constant.js
 * @Description: Coding something
 */

export const CONFIRM_STYLE = {
    DEFAULT: 'default',
    YELLOW: 'yellow',
    GAMER: 'yellow2',
};

export const CONFIRM_TYPE = {
    CONFIRM: 'confirm',
    ALERT: 'alert',
    POP: 'pop',
};
